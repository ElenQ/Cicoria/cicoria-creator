const creator = require('./lib/create.js')
const Parser  = require('argparse').ArgumentParser

parser = new Parser({
  addHelp:      true,
  description:  "Real site creator!"
})

parser.addArgument( ['-i', '--input'],{
  help: 'Input folder',
  required: true
})
parser.addArgument( ['-o', '--output'],{
  help: 'Output folder',
  required: true
})
parser.addArgument( ['-p', '--pluginFolder'],{
  help: 'Custom plugin folder for extra functionality!',
  required: false,
  defaultValue: null
})
args = parser.parseArgs()

creator.create(args['input'], args['output'], args['pluginFolder'])
