# Cicoria

Cicoria is the third or fourth iteration for a PoC of a Static Site Generator
for Blogs but with some crazy capabilities.

The idea was to make something not necessarily related with the Web
technologies we normally use. It was born from this kind of questions:

- HTTP(s):
    - Can we remove it and use other things?
    - Can we make it multiprotocol?
    - Can we use content addressed technologies?

- Client-Server model:
    - Can we reduce the server bandwith?
    - Can we make the clients serve the content too?

- HTML:
    - Can we serve other kind of markup languages and render them in a
      non-browser client?
    - Can we make clients for different platforms? Terminal clients?
    - Can we use an easy-to-read format?

- Static Sites:
    - Can we implement a search?

All of them are merged on what `Cicoria` is, but it's not built yet.

So, relax.

We'll make it happen.
