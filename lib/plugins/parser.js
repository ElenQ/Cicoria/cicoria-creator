const os   = require('os')
const yaml = require('js-yaml')

/**
 * This plugin contains all the functions needed to parse and extract all the
 * information related with the posts.
 *
 * If the plugin is overwritten, this will be used as default so it's not
 * needed to copy every function. Just replace those you want to change.
 */

function postRender(contents){
  /**
   * This function can render the post contents.
   * Can be used to change internal variables, change the filetype, convert
   * encondings... Whatever you like.
   *
   * It receives the contents as read from `fs.readFile` with no encoding
   * specified and must be returned as the `data` field of `fs.writeFile`.
   */

  // Does nothing by default. Just return the input.
  return contents
}

function metaExtract(contents){
  /**
   * This function receives the raw contents of the post files concatenated.
   * It must return a tuple with the curated post contents and the metadata
   * both as strings (will be parsed later).
   * If there's no metadata in the post the input and the curated result must
   * be the same and the metadata must be returned as undefined.
   *
   * NOTE: Metadata can be left inside the contents too. Depends on what you
   *       want to do with it.
   */

  // Extract metadata like Pandoc's YAML metadata blocks.
  //   http://pandoc.org/MANUAL.html#metadata-blocks
  let lines = contents.split( os.EOL )
  let start = lines.indexOf('---')
  let end   = lines.indexOf('...', start)

  if( start === -1 && end === -1 ){
    // No metadata -> return contents and undefined
    return [contents, undefined]
  }

  // Reads the metadata from the post
  let metadata  = lines.slice( start+1, end ).join( os.EOL )

  // Removes the metadata block from the post
  let cleancontents  = lines.slice(0, start).concat( lines.slice(end) ).join( os.EOL )

  // Returns the curated content and the metadata
  return [cleancontents, metadata]
}

function metaParse(string){
  /**
   * This function parses the metadata string. Receives the post metadata in a
   * string and must return an object of the parsed data.
   *
   * WARNING: If you change the standard metadata object structure make sure
   *          the clients are compatible with the object you return.
   */

  // Default is consider metadata a YAML file
  return yaml.safeLoad(string)
}

module.exports = {
  /* Post file related stuff */
  postExts:   ['.md', '.markdown'], // Extensions to use as Post contents
  postName:   'Contents.md',        // Output filename for Post contents
  postRender: postRender,           // Can render posts!

  /* Metadata related stuff */
  metaExtract: metaExtract,         // Extracts metadata from the Post if it's there
  metaName:    'Metadata.yaml',     // Filename for the metadata if it's separated
  metaParse:   metaParse            // Parses metadata string to an object
}
