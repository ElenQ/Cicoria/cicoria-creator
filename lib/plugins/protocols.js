const crypto    = require('crypto')

/**
 * All the protocols functions must return a tuple with the name of the
 * protocol and the resulting ID or address.
 *
 * All the exported list will be processed so all the functions in the list
 * will be appended to the final metadata.
 *
 * If this plugin is overwritten, all the protocols **must be** appended
 * because the full list will be overwritten too.
 *
 * Input is the same in all of them:
 * - `url`: is the HTTP-like URL from the root. They are internally used as file
 *   IDs.
 * - `destpath`: path where the file will be written
 * - `rawdata`: contents of the file. Read with `fs.readFile` with no encoding
 *   specified.
 */

function sha256hash(url, destpath, rawdata){
  /**
   * Example function to show how to access the contents and create
   * content-address-like string
   */
  const hash = crypto.createHash('sha256')
  hash.update( rawdata )
  return ['sha256digest', hash.digest('hex')]
}

function http(url, destpath, rawdata){
  /**
   * Default function.
   */
  return ['http', url]
}

module.exports = {
  list: [ sha256hash, // Just for testing
          http ]      // HTTP for sure, but you can remove this!!
}
