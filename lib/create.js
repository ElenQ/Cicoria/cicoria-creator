const path      = require('path')
const fs        = require('fs')
const readdirR  = require('recursive-readdir-sync')
const os        = require('os')
const mkdirp    = require('mkdirp')
const util      = require('./util')

// Plugins
var parser
var protocols


function dump_files(refs, files){
  let meta = {}
  for( let f in files ){
    let [id, src, dest] = files[f]
    let fraw = fs.readFileSync( src )

    meta[id] = {}
    for(let i in protocols.list){
      let [name, result] = protocols.list[i]( id, dest, fraw )
      meta[id][name] = result
    }

    mkdirp.sync( path.dirname(dest) )
    fs.writeFileSync( dest, fraw )
  }
  return meta
}

function process_post(refs, current){

  let content_fs = fs.readdirSync( path.join(refs.source, current) )
                     .map(    x => path.join(refs.source, current, x) )
                     .filter( x => parser.postExts.includes( path.extname(x)) )
                     .sort()

  var raw_content = '' // The raw content of the virtual file of the post
  for(let f in content_fs){
    raw_content = raw_content + os.EOL + fs.readFileSync( content_fs[f] )
  }

  // Check if metadata in contents and extract it
  let [content, metadata] = parser.metaExtract( raw_content )
  if( metadata === undefined ){
    metadata = fs.readFileSync( path.join( refs.source,
                                           current,
                                           parser.metaName ))
  }

  // Store virtualfile in a temp directory
  let tempdir = path.join( refs.temp, current )
  mkdirp.sync( tempdir )
  let postfilepath = path.join(tempdir, parser.postName)
  fs.writeFileSync( postfilepath, parser.postRender(content) )

  let relpath    = path.join( current, parser.postName )
  let targetpath = path.join( refs.target, relpath )

  let id = util.routeToURL(relpath)
  let postfile = [ id, postfilepath, targetpath ] // [ID, Path, Target Path]
  let postmeta = parser.metaParse(metadata)
  postmeta.file = id

  return [postfile, postmeta]
}

function process_filefolder(refs, current){
  let current_src = path.join( refs.source, current )
  return readdirR(current_src)
            .map( x => [ util.routeToURL(path.relative(refs.source, x)),
                         x,
                         path.join(refs.target, path.relative(refs.source,x))])
             // [ ID, Path, Target Path]
}

function parse_folder(refs, current){
  var postfile, postmeta, filelist

  let current_src = path.join( refs.source, current )
  if( !current.startsWith('_') ){
    [postfile, postmeta] = process_post(refs, current)
    filelist = fs.readdirSync(current_src)
                 .filter(x => util.isDir(path.join(current_src, x)))
                 .map( x => process_filefolder(refs, path.join(current,x) ))
                 .reduce( (x,y) => x.concat(y), []) // Flatten
                 .concat( [postfile] )
  } else {
    // Extra files which are not associated with a post
    filelist = process_filefolder(refs, current)
  }

  return [filelist, postmeta]
}

function create(source, target, pluginfolder){

  // Load plugins
  parser    = util.loadPlugin( pluginfolder, 'parser' )
  protocols = util.loadPlugin( pluginfolder, 'protocols' )

  // Make the whole thang
  var refs = {}
  refs.source = source
  refs.target = target
  refs.temp   = fs.mkdtempSync( path.join(os.tmpdir(), 'cicoria-temp-') )

  let folders = fs.readdirSync(source)
                  .filter(x => util.isDir(path.join(source, x)))
                            // ^^^^^^ Only process directories
  let files = []
  let posts = []
  for(let i in folders){
    console.log( 'Processing folder:  ', folders[i])
    let [f, p] = parse_folder( refs, folders[i] )
    if( f !== undefined ){
      files = files.concat(f)
    }
    if( p !== undefined ){
      posts.push(p)
    }
  }

  // Dump the files and retrieve info about them
  files_info = dump_files(refs, files)


  // Make the catalog and dump it
  let db = {
    posts: posts,
    files: files_info
  }
  fs.writeFileSync( path.join(target, 'Metadata.json'),
                    JSON.stringify(db,null,2) )
}

module.exports = {
  create: create
}
