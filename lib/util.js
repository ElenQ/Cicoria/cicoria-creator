const path = require('path')
const fs   = require('fs')

function routeToURL(route){
  return '/' + route.split( path.sep ).join('/')
}

function isDir(path){
    return fs.lstatSync(path).isDirectory()
}

function loadPlugin( pluginpath, modulename, default_pluginpath = './plugins/' ){
  let   default_plugin = require( default_pluginpath + modulename )

  // No pluginpath entered
  if( pluginpath === null )
    return default_plugin

  // Invalid pluginpath continue with standard
  let abspluginpath = path.resolve( pluginpath, modulename )
  try{
    var plugin = require( abspluginpath )
  }catch(e){
    console.log("PLUGIN NOT FOUND " + abspluginpath + ":  Using default")
    return default_plugin
  }

  // Found plugin, merge both plugins
  return Object.assign(default_plugin, plugin)
}

module.exports = {
  routeToURL:       routeToURL,
  isDir:            isDir,
  loadPlugin:       loadPlugin
}
